Table public.User {
  userId int [pk, increment]
  email varchar [not null, unique]
  password varchar [not null]
  name varchar [not null]
  lastName varchar
  phoneNumber varchar
}

Enum public.WalletsRole {
  USER
  ADMIN
}

Table public.UsersOnWallets {
  userId int [pk, not null, ref: > public.User.userId]
  walletId int [pk, not null, ref: > public.Wallet.walletId]
  createdAt datetime [not null, default: `now()`]
  role public.WalletsRole [not null]
  inviteAccepted boolean [not null, default: false]
  settlementBaseRate int1 [not null, default: 0]
}

Table public.Wallet {
  walletId int [pk, increment]
  title varchar(50) [not null]
  periodStart int1 [not null]
  createdAt datetime [not null, default: `now()`]
}

Table public.SettlementOverrides {
  walletId int [not null, ref: > public.Wallet.walletId]
  userId int [pk, not null, ref: > public.User.userId]
  categoryId int [pk, not null, ref: > public.Category.categoryId]
  rate int1 [not null]
  createdAt datetime [not null, default: `now()`]
}

Table public.Category {
  categoryId int [pk, increment]
  title varchar [not null]
  color varchar [default: '#b8dcff']
  archived boolean [default: false]
  walletId int [ref: > public.Wallet.walletId]
}

Table public.Transaction {
  transactionId int [pk, increment]
  amount numeric [not null]
  transactionDate datetime [not null]
  walletId int [not null, ref: > public.Wallet.walletId]
  userId int [not null, ref: > public.User.userId]
  categoryId int [not null, ref: > public.Category.categoryId]
  comment varchar
}


Table public.Goal {
  goalId int [pk, increment]
  title varchar [not null]
  walletiId int [ref: > public.Wallet.walletId]
  targetAmount numeric [default: 0]
  periodAmount numeric [default: 0]
  currentAmount numeric [default: 0]
  startDate datetime [not null]
  endDate dateTime [not null]
  order int1
  createdAt datetime [not null, default: `now()`]
}


Table public.GoalTransaction {
  goalTransactionId int [pk, increment]
  goalId int [ref: > public.Goal.goalId]
  userId int [not null, ref: > public.User.userId]
  transactionDate datetime [not null]
  amount numeric [not null]
}


