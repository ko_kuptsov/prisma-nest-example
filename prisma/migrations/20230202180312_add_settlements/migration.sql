-- AlterTable
ALTER TABLE "UsersOnWallets" ADD COLUMN     "settlementBaseRate" SMALLINT NOT NULL DEFAULT 0;

-- CreateTable
CREATE TABLE "SettlementOverrides" (
    "userId" INTEGER NOT NULL,
    "walletId" INTEGER NOT NULL,
    "categoryId" INTEGER NOT NULL,
    "rate" SMALLINT NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "SettlementOverrides_pkey" PRIMARY KEY ("userId","categoryId")
);

-- AddForeignKey
ALTER TABLE "SettlementOverrides" ADD CONSTRAINT "SettlementOverrides_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("userId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SettlementOverrides" ADD CONSTRAINT "SettlementOverrides_walletId_fkey" FOREIGN KEY ("walletId") REFERENCES "Wallet"("walletId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "SettlementOverrides" ADD CONSTRAINT "SettlementOverrides_categoryId_fkey" FOREIGN KEY ("categoryId") REFERENCES "Category"("categoryId") ON DELETE RESTRICT ON UPDATE CASCADE;
