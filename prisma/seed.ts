import {Prisma, PrismaClient} from '@prisma/client';
import {WalletsRole} from '.prisma/client';
import {defaultCategories} from '../src/resources/wallets/entities/default-categories';

// initialize Prisma Client
const prisma = new PrismaClient();

async function main() {
  await prisma.$executeRaw(Prisma.sql`SET TIME ZONE 'UTC'`);
  const createUserWithWallet = await prisma.user.upsert({
    where: { userId: 1 },
    update: {},
    create: {
      name: 'Папа свин',
      email: 'root@root.ru',
      password: '$2b$10$IteSEzWK7TBu331qLFv4OuN/nlQ/1p96i./w.d0kS8Dni5TKU5wWm',
      wallets: {
        create: [
          {
            role: WalletsRole.USER,
            inviteAccepted: false,
            wallet: {
              create: {
                periodStart: 1,
                title: 'Какой-то',
              },
            },
          },
          {
            role: WalletsRole.ADMIN,
            inviteAccepted: true,
            wallet: {
              create: {
                periodStart: 25,
                title: 'Личный',
                categories: {
                  create: [...defaultCategories],
                },
              },
            },
          },
          {
            role: WalletsRole.ADMIN,
            inviteAccepted: true,
            wallet: {
              create: {
                periodStart: 1,
                title: 'Общий',
                categories: {
                  create: [...defaultCategories],
                },
              },
            },
          },
        ],
      },
    },
  });

  const create2ndUserWithWallet = await prisma.user.upsert({
    where: { userId: 2 },
    update: {},
    create: {
      name: 'Мама свин',
      email: 'root1@root.ru',
      password: '$2b$10$IteSEzWK7TBu331qLFv4OuN/nlQ/1p96i./w.d0kS8Dni5TKU5wWm',
      wallets: {
        create: [
          {
            role: WalletsRole.USER,
            inviteAccepted: false,
            wallet: {
              connect: {
                walletId: 1,
              },
            },
          },
        ],
      },
    },
  });

  await prisma.$executeRaw(
    Prisma.sql`
    CREATE OR REPLACE FUNCTION get_random_number(INTEGER, INTEGER) RETURNS INTEGER AS $$
    DECLARE
      start_int ALIAS FOR $1;
      end_int ALIAS FOR $2;
    BEGIN
      RETURN trunc(random() * (end_int-start_int) + start_int);
    END;
    $$ LANGUAGE 'plpgsql' STRICT;`,
  );

  // generating transactions
  await fillData(1, 1);
  await fillData(1, 2);
  await fillData(2, 1);

  console.log({ createUserWithWallet });
}

async function fillData(walletId, userId) {
  await prisma.$executeRaw(
    Prisma.sql`
    INSERT INTO public."Transaction"(amount, "transactionDate", "userId", "walletId", "categoryId", comment)
    SELECT
        ROUND((random() * 2000)::numeric),
        current_date - floor((random() * 150))::int,
        ${userId},
        ${walletId},
        (CASE
            WHEN ${walletId} % 2 = 0 THEN get_random_number(15,29)
            ELSE get_random_number(1,14)
        END),
        (CASE
            WHEN i % 5 = 0 THEN NULL
            WHEN i % 3 = 0 THEN 'Билеты для отпуска'
            WHEN i % 2 = 0 THEN 'Продукты в перекрестке'
            ELSE NULL
         END) as comment
    FROM generate_series(1,300) as i;`,
  );
}

// execute the main function
main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    // close Prisma Client at the end
    await prisma.$disconnect();
  });
