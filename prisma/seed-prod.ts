// prisma/seed.ts

import {Prisma, PrismaClient} from '@prisma/client';
import {WalletsRole} from '.prisma/client';

// initialize Prisma Client
const prisma = new PrismaClient();

async function main() {
  await prisma.$executeRaw(Prisma.sql`SET TIME ZONE 'UTC'`);
  const createUserWithWallet = await prisma.user.upsert({
    where: {userId: 1},
    update: {},
    create: {
      name: 'Папа свин',
      email: 'root@root.ru',
      password: '$2b$10$IteSEzWK7TBu331qLFv4OuN/nlQ/1p96i./w.d0kS8Dni5TKU5wWm',
      wallets: {
        create: [
          {
            role: WalletsRole.ADMIN,
            wallet: {
              create: {
                walletId: 1,
                periodStart: 1,
                title: 'Общий',
                categories: {
                  create: [
                    {
                      categoryId: 1,
                      title: 'Фастфуд',
                      color: '#7ac5aa',
                    },
                    {
                      categoryId: 2,
                      title: 'Продукты',
                      color: '#6c86e2',
                    },
                    {
                      categoryId: 3,
                      title: 'Спорт',
                      color: '#e38163',
                    },
                    {
                      categoryId: 4,
                      title: 'Интернет покупки',
                    },
                  ],
                },
              },
            },
          },
          {
            role: WalletsRole.ADMIN,
            wallet: {
              create: {
                walletId: 2,
                periodStart: 25,
                title: 'Личный',
                categories: {
                  create: [
                    {
                      categoryId: 5,
                      title: 'Фастфуд',
                      color: '#7ac5aa',
                    },
                    {
                      categoryId: 6,
                      title: 'Продукты',
                      color: '#6c86e2',
                    },
                    {
                      categoryId: 7,
                      title: 'Спорт',
                      color: '#e38163',
                    },
                    {
                      categoryId: 8,
                      title: 'Интернет покупки',
                    },
                  ],
                },
              },
            },
          },
        ],
      },
    },
  });

  const create2ndUserWithWallet = await prisma.user.upsert({
    where: { userId: 2 },
    update: {},
    create: {
      name: 'Мама свин',
      email: 'root1@root.ru',
      password: '$2b$10$IteSEzWK7TBu331qLFv4OuN/nlQ/1p96i./w.d0kS8Dni5TKU5wWm1',
      wallets: {
        create: [
          {
            role: WalletsRole.USER,
            wallet: {
              connect: {
                walletId: 1,
              },
            },
          },
        ],
      },
    },
  });

  await prisma.$executeRaw(
    Prisma.sql`
    CREATE OR REPLACE FUNCTION get_random_number(INTEGER, INTEGER) RETURNS INTEGER AS $$
    DECLARE
      start_int ALIAS FOR $1;
      end_int ALIAS FOR $2;
    BEGIN
      RETURN trunc(random() * (end_int-start_int) + start_int);
    END;
    $$ LANGUAGE 'plpgsql' STRICT;`,
  );

  // generating transactions
  await prisma.$executeRaw(
    Prisma.sql`
    INSERT INTO public."Transaction"(amount, "transactionDate", "userId", "walletId", "categoryId", comment)
    SELECT
        (random() * 2000)::numeric(15,2),
        current_date - floor((random() * 150))::int,
        (CASE
            WHEN i % 3 = 0 THEN 2
            ELSE 1
        END),
        (CASE
            WHEN i % 2 = 0 THEN 1
            ELSE 2
        END),
        (CASE
            WHEN i % 2 = 0 THEN get_random_number(1,4)
            ELSE get_random_number(5,8)
        END),
        (CASE
            WHEN i % 5 = 0 THEN NULL
            WHEN i % 3 = 0 THEN 'Билеты для отпуска'
            WHEN i % 2 = 0 THEN 'Продукты в перекрестке'
            ELSE NULL
         END) as comment
    FROM generate_series(1,300) as i;`,
  );

  console.log({ createUserWithWallet });
}

// execute the main function
main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    // close Prisma Client at the end
    await prisma.$disconnect();
  });
