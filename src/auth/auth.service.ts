import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../resources/users/users.service';
import { compare, hash } from 'bcrypt';
import { UserEntity } from '../resources/users/entities/user.entity';
import { TokenPayload } from './auth.model';
import { RegisterDto } from './dto/register.dto';
import { PostgresErrorCode } from '../shared/postgress-error-codes';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async login(user: UserEntity) {
    const payload: TokenPayload = {
      userId: user.userId,
      email: user.email,
    };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }

  async getAuthenticatedUser(email: string, plainTextPassword: string) {
    const user = await this.usersService.findOneByEmail(email);
    if (!user) {
      throw new HttpException('Неверные данные логина', HttpStatus.BAD_REQUEST);
    }
    try {
      await this.verifyPassword(plainTextPassword, user.password);
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      const { password, ...rest } = user;
      return rest;
    } catch (error) {
      throw new HttpException('Неверные данные логина', HttpStatus.BAD_REQUEST);
    }
  }

  async register(
    registrationData: RegisterDto,
  ): Promise<Pick<UserEntity, 'userId'>> {
    const hashedPassword = await hash(registrationData.password, 10);
    try {
      return await this.usersService.create({
        email: registrationData.email,
        name: registrationData.name,
        password: hashedPassword,
      });
    } catch (error) {
      if (error?.code === PostgresErrorCode.UniqueViolation) {
        throw new HttpException(
          'User with that email already exists',
          HttpStatus.BAD_REQUEST,
        );
      }
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  private async verifyPassword(
    plainTextPassword: string,
    hashedPassword: string,
  ) {
    const isPasswordMatching = await compare(plainTextPassword, hashedPassword);
    if (!isPasswordMatching) {
      throw new HttpException(
        'Wrong credentials provided',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
}
