import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { jwtConstants } from './auth.constants';
import { TokenPayload } from './auth.model';
import { UsersService } from '../resources/users/users.service';
import { UserEntity } from '../resources/users/entities/user.entity';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private userService: UsersService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(payload: TokenPayload): Promise<Omit<UserEntity, 'password'>> {
    const user = await this.userService.findOneById(payload.userId);
    if (!user) {
      throw new UnauthorizedException('Wrong data');
    }
    return user;
  }
}
