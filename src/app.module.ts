import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { UsersModule } from './resources/users/users.module';
import { WalletsModule } from './resources/wallets/wallets.module';
import { TransactionsModule } from './resources/transactions/transactions.module';
import { CategoriesModule } from './resources/categories/categories.module';
import { SettlementModule } from './resources/settlement/settlement.module';
import { DashboardModule } from './resources/dashboard/dashboard.module';
import { GoalsModule } from './resources/goals/goals.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    PrismaModule,
    WalletsModule,
    TransactionsModule,
    CategoriesModule,
    SettlementModule,
    DashboardModule,
    GoalsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
