import {
  BadRequestException,
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  SetMetadata,
} from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { Request } from 'express';
import { UserEntity } from '../../resources/users/entities/user.entity';
import { Reflector } from '@nestjs/core';
import { WalletsRole } from '@prisma/client';
import { ID_KEY } from '../api-const';

export const WalletRoles = (...roles: WalletsRole[]) =>
  SetMetadata('walletsRoles', roles);

@Injectable()
export class WalletAccessGuard implements CanActivate {
  constructor(private prisma: PrismaService, private reflector: Reflector) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const walletsRoles = this.reflector.get<WalletsRole[]>(
      'walletsRoles',
      context.getHandler(),
    );
    const request = context.switchToHttp().getRequest() as Request;
    const walletId =
      +request.query['walletId'] ||
      +request.params[ID_KEY] ||
      +request.body['walletId'];
    if (!walletId || !Number.isInteger(walletId)) {
      throw new BadRequestException('Wrong parameters have been provided');
    }

    const user = request.user as UserEntity;
    const userOnWallet = await this.prisma.usersOnWallets.findFirst({
      where: {
        walletId: walletId,
        userId: user.userId,
      },
      select: {
        walletId: true,
        userId: true,
        role: true,
      },
    });

    if (!userOnWallet) {
      throw new ForbiddenException();
    }

    return (
      userOnWallet &&
      (walletsRoles
        ? walletsRoles.some((role) => role === userOnWallet.role)
        : true)
    );
  }
}
