import { UserEntity } from '../resources/users/entities/user.entity';

export interface ApiResponse<T> {
  data: T;
  error?: unknown;
}

export interface DtoWithUser<T> {
  dto: T;
  user: UserEntity;
}
