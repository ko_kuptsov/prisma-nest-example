import { IsNotEmpty, IsNumber, IsString, Min } from 'class-validator';

export class CreateGoalTransactionDto {
  @IsNotEmpty()
  @IsNumber()
  goalId: number;

  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  amount: number;

  @IsString()
  comment?: string;
}
