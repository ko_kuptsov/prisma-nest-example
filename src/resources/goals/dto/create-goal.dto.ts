import { IsDateString, IsNotEmpty, IsNumber, Length } from 'class-validator';

export class CreateGoalDto {
  @IsNotEmpty()
  @Length(5, 40)
  title: string;
  @IsNotEmpty()
  @IsNumber()
  walletId: number;
  @IsNumber()
  targetAmount: number;
  @IsNumber()
  alreadyAmount: number;
  @IsDateString()
  startDate: string;
  @IsDateString()
  endDate: string;
}
