import { Injectable } from '@nestjs/common';
import { CreateGoalDto } from './dto/create-goal.dto';
import { GoalEntity } from './entities/goal.entity';
import { PrismaService } from '../../prisma/prisma.service';
import * as dayjs from 'dayjs';
import { GoalTransactionEntity } from './entities/goal-transaction.entity';
import { CreateGoalTransactionDto } from './dto/create-goal-transaction.dto';

@Injectable()
export class GoalsService {
  constructor(private prisma: PrismaService) {}

  create(
    createGoalDto: CreateGoalDto,
    userId: number,
  ): Promise<Pick<GoalEntity, 'goalId'>> {
    return this.prisma.$transaction(async (tx) => {
      let periodAmount = 0;
      if (createGoalDto.endDate && createGoalDto.targetAmount) {
        const startDate = dayjs(createGoalDto.startDate);
        const endDate = dayjs(createGoalDto.endDate);
        const monthsInPeriod = endDate.diff(startDate, 'month') + 1;
        periodAmount = +(
          (createGoalDto.targetAmount - createGoalDto.alreadyAmount) /
          monthsInPeriod
        ).toFixed(0);
      }
      const goal = await tx.goal.create({
        data: {
          wallet: {
            connect: {
              walletId: createGoalDto.walletId,
            },
          },
          user: {
            connect: {
              userId: userId,
            },
          },
          title: createGoalDto.title,
          targetAmount: createGoalDto.targetAmount ?? 0,
          periodAmount: periodAmount,
          currentAmount: createGoalDto.alreadyAmount ?? 0,
          startDate: createGoalDto.startDate,
          endDate: createGoalDto.endDate,
        },
        select: {
          goalId: true,
          walletId: true,
        },
      });

      if (createGoalDto.alreadyAmount > 0) {
        await tx.goalTransaction.create({
          data: {
            goal: {
              connect: {
                goalId: goal.goalId,
              },
            },
            user: {
              connect: {
                userId: userId,
              },
            },
            amount: createGoalDto.alreadyAmount,
            wallet: {
              connect: {
                walletId: goal.walletId,
              },
            },
            comment: '',
          },
        });
      }
      return goal;
    });
  }

  findAll(walletId: number, from: string, to: string): Promise<GoalEntity[]> {
    return this.prisma.goal.findMany({
      where: {
        walletId: walletId,
      },
      include: {
        goalTransactions: {
          where: {
            transactionDate: {
              gte: new Date(from),
              lt: new Date(to),
            },
          },
          select: {
            userId: true,
            goalTransactionId: true,
            amount: true,
            transactionDate: true,
          },
        },
      },
      orderBy: {
        createdAt: 'desc',
      },
    });
  }

  findAllTransactions(goalId: number): Promise<GoalTransactionEntity[]> {
    return this.prisma.goalTransaction.findMany({
      where: {
        goalId: goalId,
      },
      orderBy: {
        transactionDate: 'desc',
      },
      select: {
        goalTransactionId: true,
        goalId: true,
        user: {
          select: {
            userId: true,
            name: true,
          },
        },
        walletId: true,
        amount: true,
        transactionDate: true,
        comment: true,
      },
    });
  }

  addTransaction(
    dto: CreateGoalTransactionDto,
    userId: number,
  ): Promise<GoalTransactionEntity> {
    return this.prisma.$transaction(async (tx) => {
      const goal = await tx.goal.findFirst({
        where: {
          goalId: dto.goalId,
        },
      });

      const transaction = await tx.goalTransaction.create({
        data: {
          goal: {
            connect: {
              goalId: dto.goalId,
            },
          },
          user: {
            connect: {
              userId: userId,
            },
          },
          amount: dto.amount,
          wallet: {
            connect: {
              walletId: goal.walletId,
            },
          },
          comment: dto.comment,
        },
        select: {
          goalTransactionId: true,
        },
      });

      await tx.goal.update({
        where: {
          goalId: dto.goalId,
        },
        data: {
          currentAmount: {
            increment: dto.amount,
          },
        },
      });
      return transaction;
    });
  }

  async removeTransaction(
    goalTransactionId: number,
  ): Promise<Pick<GoalTransactionEntity, 'goalTransactionId'>> {
    return this.prisma.$transaction(async (tx) => {
      const goalTransaction = await tx.goalTransaction.findFirst({
        where: {
          goalTransactionId: goalTransactionId,
        },
      });

      await tx.goalTransaction.delete({
        where: {
          goalTransactionId: goalTransactionId,
        },
      });

      await tx.goal.update({
        where: {
          goalId: goalTransaction.goalId,
        },
        data: {
          currentAmount: {
            decrement: goalTransaction.amount,
          },
        },
      });
      return {
        goalTransactionId,
      };
    });
  }

  remove(goalId: number): Promise<Pick<GoalEntity, 'goalId'>> {
    return this.prisma.goal.delete({
      where: {
        goalId: goalId,
      },
      select: {
        goalId: true,
      },
    });
  }
}
