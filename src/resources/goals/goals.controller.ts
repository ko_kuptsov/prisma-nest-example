import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { GoalsService } from './goals.service';
import { CreateGoalDto } from './dto/create-goal.dto';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ApiResponse } from '../../shared/api-model';
import { GoalEntity } from './entities/goal.entity';
import { GoalTransactionEntity } from './entities/goal-transaction.entity';
import { CreateGoalTransactionDto } from './dto/create-goal-transaction.dto';
import { User } from '../../shared/decorators/user';
import { UserEntity } from '../users/entities/user.entity';
import {
  WalletAccessGuard,
  WalletRoles,
} from '../../shared/guards/wallet-access.guard';
import { WalletsRole } from '@prisma/client';

@Controller('goals')
export class GoalsController {
  constructor(private readonly goalsService: GoalsService) {}

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Post()
  async create(
    @Body() createGoalDto: CreateGoalDto,
    @User() user: UserEntity,
  ): Promise<ApiResponse<Pick<GoalEntity, 'goalId'>>> {
    const res = await this.goalsService.create(createGoalDto, user.userId);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('')
  async getGoals(
    @Query('walletId', ParseIntPipe) walletId: number,
    @Query('from') from: string,
    @Query('to') to: string,
  ): Promise<ApiResponse<GoalEntity[]>> {
    if (!from || !to || !walletId) {
      throw new BadRequestException(
        "Missing 'from' or 'to' or 'walletId' properties.",
      );
    }
    const res = await this.goalsService.findAll(walletId, from, to);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('transactions')
  async getGoalTransactions(
    @Query('goalId', ParseIntPipe) goalId: number,
  ): Promise<ApiResponse<GoalTransactionEntity[]>> {
    if (!goalId) {
      throw new BadRequestException("Missing 'goalId' property.");
    }
    const res = await this.goalsService.findAllTransactions(goalId);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post('transactions')
  async addGoalTransaction(
    @Body() createTransactionDto: CreateGoalTransactionDto,
    @User() user: UserEntity,
  ): Promise<ApiResponse<GoalTransactionEntity>> {
    const res = await this.goalsService.addTransaction(
      createTransactionDto,
      user.userId,
    );
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Delete('transactions/:goalTransactionId')
  async removeGoalTransaction(
    @Param('goalTransactionId', ParseIntPipe) goalTransactionId: number,
  ): Promise<ApiResponse<GoalTransactionEntity>> {
    const res = await this.goalsService.removeTransaction(goalTransactionId);
    return {
      data: res,
    };
  }

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard)
  @Delete(':goalId')
  async remove(
    @Param('goalId', ParseIntPipe) id: number,
  ): Promise<ApiResponse<Pick<GoalEntity, 'goalId'>>> {
    const res = await this.goalsService.remove(id);
    return {
      data: res,
    };
  }
}
