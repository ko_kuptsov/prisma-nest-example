import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { PrismaService } from '../../prisma/prisma.service';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  create(createUserDto: CreateUserDto): Promise<Pick<UserEntity, 'userId'>> {
    return this.prisma.user.create({
      data: {
        email: createUserDto.email,
        password: createUserDto.password,
        name: createUserDto.name,
        lastName: createUserDto.lastName,
      },
      select: {
        userId: true,
      },
    });
  }

  findAll() {
    return `This action returns all users`;
  }

  findOne(id: number): Promise<UserEntity> {
    return this.prisma.user.findUnique({ where: { userId: id } });
  }

  findOneByEmail(email: string): Promise<UserEntity> {
    return this.prisma.user.findUnique({ where: { email: email } });
  }

  findOneById(id: number): Promise<Omit<UserEntity, 'password'>> {
    return this.prisma.user.findUnique({
      where: { userId: id },
      select: {
        userId: true,
        name: true,
        email: true,
        lastName: true,
        phoneNumber: true,
      },
    });
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    return `This action updates a #${id} user`;
  }
}
