import { User } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';

export class UserEntity implements User {
  @ApiProperty()
  userId: number;

  @ApiProperty()
  name: string;

  @Exclude({ toPlainOnly: true })
  @ApiProperty()
  password: string;

  @ApiProperty()
  email: string;

  @ApiProperty()
  lastName: string | null;

  @ApiProperty()
  phoneNumber: string | null;
}
