import { WalletEntity } from '../entities/wallet.entity';
import { CategoryEntity } from '../../categories/entities/category.entity';
import { ApiProperty } from '@nestjs/swagger';

export class WalletMeta {
  @ApiProperty()
  walletId: number;
  wallet: WalletEntity & { categories: CategoryEntity[] };
  role: 'ADMIN' | 'USER';
  inviteAccepted: boolean;
}
