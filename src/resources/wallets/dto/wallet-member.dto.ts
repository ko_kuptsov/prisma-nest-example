export class WalletMemberDto {
  walletId: number;
  role: 'ADMIN' | 'USER';
  createdAt: Date;
  inviteAccepted: boolean;
  user: {
    userId: number;
    name: string;
  };
}
