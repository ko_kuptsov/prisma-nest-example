import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { WalletEntity } from './entities/wallet.entity';
import { PrismaService } from '../../prisma/prisma.service';
import { CategoriesService } from '../categories/categories.service';
import { WalletMemberDto } from './dto/wallet-member.dto';
import { WalletMeta } from './dto/wallet-meta';
import { InviteMemberDto } from './dto/invite-member.dto';
import { AcceptInviteDto } from './dto/accept-invite.dto';
import { defaultCategories } from './entities/default-categories';

@Injectable()
export class WalletsService {
  constructor(
    private prisma: PrismaService,
    private categoriesService: CategoriesService,
  ) {}

  async create(userId: number, createWalletDto: CreateWalletDto) {
    return this.prisma.usersOnWallets.create({
      data: {
        wallet: {
          create: {
            title: createWalletDto.title,
            periodStart: createWalletDto.periodStart,
            categories: {
              create: [...defaultCategories],
            },
          },
        },
        role: 'ADMIN',
        user: {
          connect: {
            userId: userId,
          },
        },
        inviteAccepted: true,
      },
      select: {
        walletId: true,
      },
    });
  }

  async inviteMember(walletId: number, inviteMemberDto: InviteMemberDto) {
    const user = await this.prisma.user.findFirst({
      where: {
        email: inviteMemberDto.email,
      },
    });

    if (!user) {
      throw new BadRequestException();
    }

    return this.prisma.usersOnWallets.create({
      data: {
        inviteAccepted: false,
        role: 'USER',
        walletId: walletId,
        userId: user.userId,
      },
      select: {
        createdAt: true,
      },
    });
  }

  async acceptInvite(walletId: number, acceptInviteDto: AcceptInviteDto) {
    return this.prisma.usersOnWallets.update({
      where: {
        userId_walletId: {
          userId: acceptInviteDto.userId,
          walletId: walletId,
        },
      },
      data: {
        inviteAccepted: true,
      },
      select: {
        inviteAccepted: true,
      },
    });
  }

  findMembers(walletId: number): Promise<WalletMemberDto[]> {
    return this.prisma.usersOnWallets.findMany({
      where: {
        walletId: walletId,
      },
      select: {
        walletId: true,
        role: true,
        createdAt: true,
        inviteAccepted: true,
        user: {
          select: {
            name: true,
            userId: true,
          },
        },
      },
    });
  }

  findAll() {
    return `This action returns all wallets`;
  }

  findOne(id: number) {
    return this.prisma.wallet.findUnique({
      where: {
        walletId: id,
      },
      include: {
        categories: {
          select: {
            categoryId: true,
            title: true,
            color: true,
            walletId: true,
          },
        },
      },
    });
  }

  async findByUserId(userId: number): Promise<WalletEntity[]> {
    return this.prisma.wallet.findMany({
      where: {
        users: {
          some: {
            userId: userId,
          },
        },
      },
      include: {
        categories: {
          select: {
            categoryId: true,
            title: true,
            color: true,
            walletId: true,
          },
        },
      },
    });
  }

  async findByUserWalletMeta(userId: number): Promise<WalletMeta[]> {
    return this.prisma.usersOnWallets.findMany({
      where: {
        userId: userId,
      },
      select: {
        role: true,
        inviteAccepted: true,
        walletId: true,
        wallet: {
          include: {
            categories: {
              where: {
                archived: false,
              },
            },
          },
        },
      },
    });
  }

  async findWalletMeta(walletId: number): Promise<WalletMeta> {
    return this.prisma.usersOnWallets.findFirst({
      where: {
        walletId: walletId,
      },
      select: {
        role: true,
        inviteAccepted: true,
        walletId: true,
        wallet: {
          include: {
            categories: true,
          },
        },
      },
    });
  }

  update(id: number, updateWalletDto: UpdateWalletDto) {
    return `This action updates a #${id} wallet`;
  }

  remove(id: number) {
    return `This action removes a #${id} wallet`;
  }
}
