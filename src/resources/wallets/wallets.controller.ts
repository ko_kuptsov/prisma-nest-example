import {
  Body,
  Controller,
  Delete,
  ForbiddenException,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { WalletsService } from './wallets.service';
import { CreateWalletDto } from './dto/create-wallet.dto';
import { UpdateWalletDto } from './dto/update-wallet.dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { WalletEntity } from './entities/wallet.entity';
import { Request } from 'express';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ApiResponse } from '../../shared/api-model';
import { WalletMemberDto } from './dto/wallet-member.dto';
import { WalletMeta } from './dto/wallet-meta';
import { User } from '../../shared/decorators/user';
import { UserEntity } from '../users/entities/user.entity';
import { API_ID_KEY, ID_KEY } from '../../shared/api-const';
import {
  WalletAccessGuard,
  WalletRoles,
} from '../../shared/guards/wallet-access.guard';
import { WalletsRole } from '@prisma/client';

@ApiTags('wallets')
@Controller('wallets')
export class WalletsController {
  constructor(private readonly walletsService: WalletsService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Body() createWalletDto: CreateWalletDto,
    @User() user: UserEntity,
  ) {
    const res = await this.walletsService.create(user.userId, createWalletDto);
    return {
      data: res,
    };
  }

  @ApiOkResponse({ type: WalletEntity, isArray: true })
  @UseGuards(JwtAuthGuard)
  @Get('all/meta')
  async findByUserIdNew(
    @Query('userId', ParseIntPipe) userId: number,
    @User() user: UserEntity,
  ): Promise<ApiResponse<WalletMeta[]>> {
    if (user.userId !== userId) {
      throw new ForbiddenException();
    }
    const wallets = await this.walletsService.findByUserWalletMeta(userId);
    return {
      data: wallets,
    };
  }

  @ApiOkResponse({ type: WalletEntity, isArray: true })
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get(`${API_ID_KEY}/meta`)
  async getWalletMeta(
    @Param(ID_KEY, ParseIntPipe) id: number,
  ): Promise<ApiResponse<WalletMeta>> {
    const wallets = await this.walletsService.findWalletMeta(id);
    return {
      data: wallets,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get(`${API_ID_KEY}/members`)
  async findMembers(
    @Param(ID_KEY, ParseIntPipe) id: number,
  ): Promise<ApiResponse<WalletMemberDto[]>> {
    const wallets = await this.walletsService.findMembers(id);
    return {
      data: wallets,
    };
  }

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Post(`${API_ID_KEY}/invite`)
  async inviteMember(
    @Param(ID_KEY, ParseIntPipe) id: number,
    @Req() request: Request,
  ): Promise<ApiResponse<{ createdAt: Date }>> {
    const res = await this.walletsService.inviteMember(id, request.body);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Post(`${API_ID_KEY}/accept-invite`)
  async acceptInvite(
    @Param(ID_KEY, ParseIntPipe) id: number,
    @Req() request: Request,
  ): Promise<ApiResponse<{ inviteAccepted: boolean }>> {
    const res = await this.walletsService.acceptInvite(id, request.body);
    return {
      data: res,
    };
  }

  @Get(`${API_ID_KEY}`)
  async findOne(
    @Param(ID_KEY, ParseIntPipe) id: number,
  ): Promise<ApiResponse<WalletEntity>> {
    const wallet = await this.walletsService.findOne(id);
    return {
      data: wallet,
    };
  }

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Patch(`${API_ID_KEY}`)
  update(@Param(ID_KEY) id: string, @Body() updateWalletDto: UpdateWalletDto) {
    return this.walletsService.update(+id, updateWalletDto);
  }

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Delete(`${API_ID_KEY}`)
  remove(@Param(ID_KEY) id: string) {
    return this.walletsService.remove(+id);
  }
}
