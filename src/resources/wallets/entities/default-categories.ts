export const defaultCategories = [
  {
    title: 'Рестораны',
    color: '#a8cef1',
  },
  {
    title: 'Продукты',
    color: '#8dda71',
  },
  {
    title: 'Интернет покупки',
    color: '#e29398',
  },
  {
    title: 'Машина',
    color: '#fcc068',
  },
  {
    title: 'Одежда и Обувь',
    color: '#dab3f9',
  },
  {
    title: 'Развлечения',
    color: '#fee797',
  },
  {
    title: 'Спорт',
    color: '#ea97c4',
  },
  {
    title: 'Подарки',
    color: '#bd65a4',
  },
  {
    title: 'Здоровье',
    color: '#fcbb14',
  },
  {
    title: 'Животные',
    color: '#7b439e',
  },
  {
    title: 'Путешествие',
    color: '#ff8a00',
  },
  {
    title: 'Транспорт',
    color: '#b8474e',
  },
  {
    title: 'Дом',
    color: '#34b41f',
  },
  {
    title: 'Прочее',
    color: '#3682db',
  },
];
