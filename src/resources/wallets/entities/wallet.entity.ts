import { Wallet } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class WalletEntity implements Wallet {
  @ApiProperty()
  title: string;
  @ApiProperty()
  walletId: number;
  @ApiProperty()
  createdAt: Date;
  @ApiProperty()
  periodStart: number;
}
