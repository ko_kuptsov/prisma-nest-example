import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { CategoriesService } from './categories.service';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { ApiResponse } from '../../shared/api-model';
import { CategoryEntity } from './entities/category.entity';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { CategoryGroupByAmountDto } from './dto/category-group-by-amount.dto';
import { WalletAccessGuard } from '../../shared/guards/wallet-access.guard';
import { API_ID_KEY, ID_KEY } from '../../shared/api-const';

@Controller('categories')
export class CategoriesController {
  constructor(private readonly categoriesService: CategoriesService) {}

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Post()
  async create(
    @Body() createCategoryDto: CreateCategoryDto,
  ): Promise<ApiResponse<Pick<CategoryEntity, 'categoryId'>>> {
    const data = await this.categoriesService.create(createCategoryDto);
    return {
      data: data,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get()
  async findAll(
    @Query('walletId', ParseIntPipe) walletId: number,
  ): Promise<ApiResponse<CategoryEntity[]>> {
    if (!walletId) {
      throw new BadRequestException(
        `Categories for wallet with id = ${walletId} not found`,
      );
    }
    const data = await this.categoriesService.findByWalletId(walletId);
    return {
      data: data,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('group-by-id')
  async groupByIdWithAmount(
    @Query('walletId', ParseIntPipe) walletId: number,
    @Query('from') from: string,
    @Query('to') to: string,
  ): Promise<ApiResponse<CategoryGroupByAmountDto[]>> {
    if (!from || !to || !walletId) {
      throw new BadRequestException(
        "Missing 'from' or 'to' or 'walletId' properties.",
      );
    }
    const res = await this.categoriesService.groupByAmount(walletId, from, to);
    return {
      data: res,
    };
  }

  // todo guard
  @UseGuards(JwtAuthGuard)
  @Put(API_ID_KEY)
  async update(
    @Param(ID_KEY, ParseIntPipe) id: number,
    @Body() updateCategoryDto: UpdateCategoryDto,
  ): Promise<ApiResponse<Pick<CategoryEntity, 'categoryId'>>> {
    const data = await this.categoriesService.update(id, updateCategoryDto);
    return {
      data: data,
    };
  }

  // todo guard
  @UseGuards(JwtAuthGuard)
  @Delete(API_ID_KEY)
  async remove(
    @Param(ID_KEY, ParseIntPipe) id: number,
  ): Promise<ApiResponse<Pick<CategoryEntity, 'categoryId'>>> {
    const data = await this.categoriesService.remove(id);
    return {
      data: data,
    };
  }
}
