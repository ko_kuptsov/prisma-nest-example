import { Category } from '@prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class CategoryEntity implements Category {
  @ApiProperty()
  categoryId: number;
  @ApiProperty()
  color: string;
  @ApiProperty()
  title: string;
  @ApiProperty()
  walletId: number;
  @ApiProperty()
  archived: boolean;
}
