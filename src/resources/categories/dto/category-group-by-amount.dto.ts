export class CategoryGroupByAmountDto {
  categoryId: number;
  title: string;
  color: string;
  walletId: number;
  amountsum: string;
}
