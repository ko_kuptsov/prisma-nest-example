import { Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { CategoryEntity } from './entities/category.entity';
import { PrismaService } from '../../prisma/prisma.service';
import { CategoryGroupByAmountDto } from './dto/category-group-by-amount.dto';

@Injectable()
export class CategoriesService {
  constructor(private prisma: PrismaService) {}

  create(
    createCategoryDto: CreateCategoryDto,
  ): Promise<Pick<CategoryEntity, 'categoryId'>> {
    return this.prisma.category.create({
      data: {
        wallet: {
          connect: {
            walletId: createCategoryDto.walletId,
          },
        },
        title: createCategoryDto.title,
        color: createCategoryDto.color,
      },
      select: {
        categoryId: true,
      },
    });
  }

  async findByWalletId(walletId): Promise<CategoryEntity[]> {
    return this.prisma.category.findMany({
      where: {
        walletId: walletId,
        archived: false,
      },
      orderBy: {
        categoryId: 'asc',
      },
    });
  }

  async groupByAmount(
    walletId: number,
    from: string,
    to: string,
  ): Promise<CategoryGroupByAmountDto[]> {
    // prettier-ignore
    return this.prisma.$queryRaw<CategoryGroupByAmountDto[]>`
        SELECT category."categoryId", title, color, category."walletId", SUM(trs."amount") as amountSum
        FROM public."Category" as category 
        INNER JOIN public."Transaction" as trs ON trs."categoryId" = category."categoryId" 
        WHERE (category."walletId" = ${walletId} AND trs."transactionDate" >= ${new Date(
      from,
    )} 
            AND trs."transactionDate" < ${new Date(to)})
        GROUP BY category."categoryId"
        ORDER BY amountSum DESC NULLS LAST
        `;
  }

  update(
    id: number,
    updateCategoryDto: UpdateCategoryDto,
  ): Promise<Pick<CategoryEntity, 'categoryId'>> {
    return this.prisma.category.update({
      where: {
        categoryId: id,
      },
      data: {
        title: updateCategoryDto.title,
        color: updateCategoryDto.color,
      },
      select: {
        categoryId: true,
      },
    });
  }

  async remove(id: number): Promise<Pick<CategoryEntity, 'categoryId'>> {
    const transactions = await this.prisma.transaction.findMany({
      where: {
        categoryId: id,
      },
      select: {
        transactionId: true,
      },
    });
    if (transactions.length) {
      return this.prisma.category.update({
        where: {
          categoryId: id,
        },
        data: {
          archived: true,
        },
        select: {
          categoryId: true,
        },
      });
    } else {
      return this.prisma.category.delete({
        where: {
          categoryId: id,
        },
        select: {
          categoryId: true,
        },
      });
    }
  }
}
