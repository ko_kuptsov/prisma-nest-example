import {
  Controller,
  Get,
  ParseIntPipe,
  Query,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { TransactionsService } from '../transactions/transactions.service';
import { ApiResponse } from '../../shared/api-model';
import { WalletAccessGuard } from '../../shared/guards/wallet-access.guard';

@Controller('dashboard')
export class DashboardController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('expenses-total')
  async expensesTotal(
    @Query('walletId', ParseIntPipe) walletId: number,
    @Query('from') from: string,
    @Query('to') to: string,
  ): Promise<ApiResponse<{ sum: number }>> {
    return {
      data: (
        await this.transactionsService.calculateExpensesTotal(
          walletId,
          from,
          to,
        )
      )[0],
    };
  }
}
