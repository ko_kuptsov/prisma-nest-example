import { Injectable } from '@nestjs/common';
import { PrismaService } from '../../prisma/prisma.service';
import { SettlementDto } from './dto/settlement.dto';
import { SettlementUpdateDto } from './dto/settlement-update.dto';
import { TransactionsService } from '../transactions/transactions.service';
import {
  SettlementCalculatedDto,
  SettlementCalculateDto,
} from './dto/settlement-calculated.dto';
import { CategoriesService } from '../categories/categories.service';
import { CategoryGroupByAmountDto } from '../categories/dto/category-group-by-amount.dto';

@Injectable()
export class SettlementService {
  constructor(
    private prisma: PrismaService,
    private transactionsService: TransactionsService,
    private categoriesService: CategoriesService,
  ) {}

  async getSettlementInfo(walletId: number): Promise<SettlementDto> {
    const overrides = await this.prisma.settlementOverrides.findMany({
      where: {
        walletId: walletId,
      },
      select: {
        category: true,
        userId: true,
        rate: true,
      },
      orderBy: {
        userId: 'asc',
      },
    });

    const baseRates = await this.prisma.usersOnWallets.findMany({
      where: {
        walletId: walletId,
      },
      select: {
        user: {
          select: {
            userId: true,
            name: true,
          },
        },
        settlementBaseRate: true,
      },
      orderBy: {
        userId: 'asc',
      },
    });

    return {
      walletId: walletId,
      baseUserRates: baseRates.map((it) => {
        return {
          user: it.user,
          rate: it.settlementBaseRate,
        };
      }),
      overrides: overrides,
    };
  }

  async updateSettlement(
    dto: SettlementUpdateDto,
  ): Promise<{ walletId: number }> {
    return await this.prisma.$transaction(async (tx) => {
      for (const baseUserRate of dto.baseUserRates) {
        await tx.usersOnWallets.update({
          where: {
            userId_walletId: {
              userId: baseUserRate.userId,
              walletId: dto.walletId,
            },
          },
          data: {
            settlementBaseRate: baseUserRate.rate,
          },
        });
      }

      return tx.wallet.update({
        where: {
          walletId: dto.walletId,
        },
        data: {
          settlementOverrides: {
            deleteMany: {
              NOT: dto.overrides.map((ov) => {
                return {
                  categoryId: ov.categoryId,
                  userId: ov.userId,
                };
              }),
            },
            upsert: dto.overrides.map((ov) => {
              return {
                where: {
                  userId_categoryId: {
                    userId: ov.userId,
                    categoryId: ov.categoryId,
                  },
                },
                create: ov,
                update: ov,
              };
            }),
          },
        },
        select: {
          walletId: true,
        },
      });
    });
  }

  async calculateSettlements(
    req: SettlementCalculateDto,
  ): Promise<SettlementCalculatedDto> {
    const [totalByUsers, categoriesTotal, settlementsConfig] = [
      await this.transactionsService.groupByUsersWithAmount(
        req.walletId,
        req.from,
        req.to,
        'users."userId"',
      ),
      await this.categoriesService.groupByAmount(
        req.walletId,
        req.from,
        req.to,
      ),
      await this.getSettlementInfo(req.walletId),
    ];

    const [onlyOverrides, withoutOverrides]: [
      CategoryGroupByAmountDto[],
      CategoryGroupByAmountDto[],
    ] = categoriesTotal.reduce(
      (res, item) => {
        res[
          settlementsConfig.overrides.find(
            (c) => c.category.categoryId === item.categoryId,
          )
            ? 0
            : 1
        ].push(item);
        return res;
      },
      [[], []],
    );

    const sumWithoutOverrides = withoutOverrides.reduce((res, item) => {
      return res + +Number.parseInt(item.amountsum).toFixed(2);
    }, 0);

    const resultUserMap = new Map<number, number>();

    settlementsConfig.baseUserRates.forEach((bU) => {
      if (!resultUserMap.has(bU.user.userId)) {
        resultUserMap.set(bU.user.userId, 0);
      }

      resultUserMap.set(
        bU.user.userId,
        resultUserMap.get(bU.user.userId) +
          +(sumWithoutOverrides * (bU.rate / 100)).toFixed(2),
      );
    });

    settlementsConfig.overrides.forEach((ov) => {
      const categorySum = onlyOverrides.find(
        (o) => o.categoryId === ov.category.categoryId,
      ).amountsum;

      resultUserMap.set(
        ov.userId,
        +(
          resultUserMap.get(ov.userId) +
          +categorySum * (ov.rate / 100)
        ).toFixed(2),
      );
    });

    console.log(resultUserMap);

    return {
      settlements: Array.from(resultUserMap.keys()).map((userId) => {
        return {
          userId: userId,
          sum: -(
            totalByUsers.find((tU) => tU.userId === userId).amountsum -
            resultUserMap.get(userId)
          ).toFixed(2),
        };
      }),
    };
  }
}
