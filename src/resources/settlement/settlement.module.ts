import { Module } from '@nestjs/common';
import { SettlementService } from './settlement.service';
import { SettlementController } from './settlement.controller';
import { PrismaModule } from '../../prisma/prisma.module';
import { TransactionsModule } from '../transactions/transactions.module';
import { CategoriesModule } from '../categories/categories.module';

@Module({
  imports: [PrismaModule, TransactionsModule, CategoriesModule],
  controllers: [SettlementController],
  providers: [SettlementService],
})
export class SettlementModule {}
