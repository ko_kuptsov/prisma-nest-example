import { IsArray, IsNotEmpty, IsNumber } from 'class-validator';

export class SettlementUpdateDto {
  @IsNotEmpty()
  @IsNumber()
  walletId: number;

  @IsArray()
  baseUserRates: {
    userId: number;
    rate: number;
  }[];

  @IsArray()
  overrides: {
    categoryId: number;
    userId: number;
    rate: number;
  }[];
}
