import { CategoryEntity } from '../../categories/entities/category.entity';
import { UserEntity } from '../../users/entities/user.entity';

export interface SettlementBaseUserRates {
  user: Pick<UserEntity, 'userId' | 'name'>;
  rate: number;
}

export interface SettlementOverrides {
  category: CategoryEntity;
  userId: number;
  rate: number;
}

export class SettlementDto {
  walletId: number;
  baseUserRates: SettlementBaseUserRates[];
  overrides: SettlementOverrides[];
}
