import { IsDateString, IsNotEmpty, IsNumber } from 'class-validator';

export class SettlementCalculatedDto {
  settlements: {
    userId: number;
    sum: number;
  }[];
}

export class SettlementCalculateDto {
  @IsNotEmpty()
  @IsNumber()
  walletId: number;

  @IsNotEmpty()
  @IsDateString()
  from: string;

  @IsNotEmpty()
  @IsDateString()
  to: string;
}
