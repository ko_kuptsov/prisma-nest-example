import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { SettlementService } from './settlement.service';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { ApiResponse } from '../../shared/api-model';
import { SettlementDto } from './dto/settlement.dto';
import { SettlementUpdateDto } from './dto/settlement-update.dto';
import {
  SettlementCalculatedDto,
  SettlementCalculateDto,
} from './dto/settlement-calculated.dto';
import {
  WalletAccessGuard,
  WalletRoles,
} from '../../shared/guards/wallet-access.guard';
import { WalletsRole } from '@prisma/client';

@Controller('settlement')
export class SettlementController {
  constructor(private readonly settlementService: SettlementService) {}

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('info')
  async getInfo(
    @Query('walletId', ParseIntPipe) walletId: number,
  ): Promise<ApiResponse<SettlementDto>> {
    if (!walletId) {
      throw new BadRequestException('walletId is required parameter');
    }
    const data = await this.settlementService.getSettlementInfo(walletId);
    return {
      data: data,
    };
  }

  @WalletRoles(WalletsRole.ADMIN)
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Put()
  async updateSettlement(
    @Body() request: SettlementUpdateDto,
  ): Promise<ApiResponse<{ walletId: number }>> {
    const data = await this.settlementService.updateSettlement(request);
    return {
      data: data,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @HttpCode(200)
  @Post('calculate')
  async calculateSettlements(
    @Body() req: SettlementCalculateDto,
  ): Promise<ApiResponse<SettlementCalculatedDto>> {
    const data = await this.settlementService.calculateSettlements(req);
    return {
      data: data,
    };
  }
}
