import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { ApiCreatedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ApiResponse } from '../../shared/api-model';
import { TransactionEntity } from './entities/transaction.entity';
import { JwtAuthGuard } from '../../auth/jwt-auth.guard';
import { TransactionGroupByUserDto } from './dto/group-by-user.dto';
import { User } from '../../shared/decorators/user';
import { UserEntity } from '../users/entities/user.entity';
import { WalletAccessGuard } from '../../shared/guards/wallet-access.guard';

@ApiTags('transactions')
@Controller('transactions')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}

  @ApiCreatedResponse()
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Post()
  async create(
    @Body() createTransactionDto: CreateTransactionDto,
    @User() user: UserEntity,
  ): Promise<ApiResponse<number>> {
    const res = await this.transactionsService.create(
      createTransactionDto,
      user.userId,
    );
    return {
      data: res.transactionId,
    };
  }

  @ApiOkResponse({ type: TransactionEntity, isArray: true })
  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('all')
  async findAll(
    @Query('walletId', ParseIntPipe) walletId: number,
    @Query('from') from: string,
    @Query('to') to: string,
  ): Promise<ApiResponse<TransactionEntity[]>> {
    if (!from || !to || !walletId) {
      throw new BadRequestException(
        "Missing 'from' or 'to' or 'walletId' properties.",
      );
    }
    const res = await this.transactionsService.findAll(walletId, from, to);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard, WalletAccessGuard)
  @Get('group-by-users')
  async groupByUsersWithAmount(
    @Query('walletId', ParseIntPipe) walletId: number,
    @Query('from') from: string,
    @Query('to') to: string,
  ): Promise<ApiResponse<TransactionGroupByUserDto[]>> {
    if (!from || !to || !walletId) {
      throw new BadRequestException(
        "Missing 'from' or 'to' or 'walletId' properties.",
      );
    }
    const res = await this.transactionsService.groupByUsersWithAmount(
      walletId,
      from,
      to,
      'amountSum',
    );
    return {
      data: res,
    };
  }

  @ApiOkResponse({ type: TransactionEntity })
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ApiResponse<TransactionEntity>> {
    if (!Number.isInteger(id)) {
      throw new BadRequestException('Wrong transactionId parameter');
    }
    const res = await this.transactionsService.findOne(id);
    return {
      data: res,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Put(':id')
  async update(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateTransactionDto: UpdateTransactionDto,
  ): Promise<ApiResponse<number>> {
    const res = await this.transactionsService.update(id, updateTransactionDto);
    return {
      data: res.transactionId,
    };
  }

  @Delete(':id')
  async remove(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ApiResponse<number>> {
    const res = await this.transactionsService.remove(id);
    return {
      data: res.transactionId,
    };
  }
}
