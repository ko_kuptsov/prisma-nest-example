import { Injectable, Logger } from '@nestjs/common';
import { CreateTransactionDto } from './dto/create-transaction.dto';
import { UpdateTransactionDto } from './dto/update-transaction.dto';
import { PrismaService } from '../../prisma/prisma.service';
import { TransactionEntity } from './entities/transaction.entity';
import { TransactionGroupByUserDto } from './dto/group-by-user.dto';

@Injectable()
export class TransactionsService {
  private logger = new Logger(TransactionsService.name);

  constructor(private prisma: PrismaService) {}

  create(createTransactionDto: CreateTransactionDto, userId: number) {
    return this.prisma.transaction.create({
      data: {
        transactionDate: createTransactionDto.transactionDate,
        comment: createTransactionDto.comment,
        amount: createTransactionDto.amount,
        wallet: {
          connect: {
            walletId: createTransactionDto.walletId,
          },
        },
        category: {
          connect: {
            categoryId: createTransactionDto.categoryId,
          },
        },
        user: {
          connect: {
            userId: userId,
          },
        },
      },
    });
  }

  async calculateExpensesTotal(
    walletId: number,
    from: string,
    to: string,
  ): Promise<{ sum: number }> {
    return this.prisma.$queryRaw<{ sum: number }>`
      SELECT SUM(amount)
      FROM public."Transaction"
      WHERE ("walletId" = ${walletId} AND "transactionDate" >= ${new Date(from)}
        AND "transactionDate" < ${new Date(to)})`;
  }

  async findAll(
    walletId: number,
    from: string,
    to: string,
  ): Promise<TransactionEntity[]> {
    return this.prisma.transaction.findMany({
      orderBy: {
        transactionDate: 'desc',
      },
      where: {
        walletId: walletId,
        AND: [
          {
            transactionDate: {
              gte: new Date(from),
              lt: new Date(to),
            },
          },
        ],
      },
      include: {
        category: {
          select: {
            categoryId: true,
            title: true,
            color: true,
          },
        },
        user: {
          select: {
            userId: true,
            name: true,
          },
        },
      },
    });
  }

  async findOne(transactionId: number): Promise<TransactionEntity> {
    return this.prisma.transaction.findUniqueOrThrow({
      where: {
        transactionId: transactionId,
      },
      include: {
        category: {
          select: {
            categoryId: true,
            title: true,
            color: true,
          },
        },
      },
    });
  }

  async groupByUsersWithAmount(
    walletId: number,
    from: string,
    to: string,
    orderBy = 'amountSum',
  ): Promise<TransactionGroupByUserDto[]> {
    // prettier-ignore
    return this.prisma.$queryRaw<TransactionGroupByUserDto[]>`
      SELECT users."userId", name, SUM(trs."amount") as amountSum
      FROM public."User" as users
      LEFT JOIN public."Transaction" as trs ON trs."userId" = users."userId"
      WHERE (trs."walletId" = ${walletId} AND trs."transactionDate" >= ${new Date(from)}
        AND trs."transactionDate" < ${new Date(to)})
      GROUP BY users."userId"
      ORDER BY ${orderBy} DESC NULLS LAST`;
  }

  update(id: number, updateTransactionDto: UpdateTransactionDto) {
    return this.prisma.transaction.update({
      where: {
        transactionId: id,
      },
      data: updateTransactionDto,
      select: {
        transactionId: true,
      },
    });
  }

  remove(id: number) {
    return this.prisma.transaction.delete({
      where: {
        transactionId: id,
      },
      select: {
        transactionId: true,
      },
    });
  }
}
