import { Module } from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionsController } from './transactions.controller';
import { PrismaModule } from '../../prisma/prisma.module';
import { WalletAccessGuard } from '../../shared/guards/wallet-access.guard';

@Module({
  imports: [PrismaModule],
  controllers: [TransactionsController],
  providers: [TransactionsService, WalletAccessGuard],
  exports: [TransactionsService],
})
export class TransactionsModule {}
