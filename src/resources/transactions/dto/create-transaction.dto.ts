import { ApiProperty } from '@nestjs/swagger';
import { Prisma } from '.prisma/client';
import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class CreateTransactionDto {
  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  walletId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  categoryId: number;

  @IsNotEmpty()
  @IsNumber()
  @ApiProperty()
  amount: Prisma.Decimal;

  @IsNotEmpty()
  @IsDateString()
  @ApiProperty()
  transactionDate: string;

  @IsString()
  @ApiProperty()
  comment?: string | null;
}
