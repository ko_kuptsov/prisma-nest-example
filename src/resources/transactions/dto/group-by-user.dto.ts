export class TransactionGroupByUserDto {
  userId: number;
  name: string;
  amountsum: number;
}
