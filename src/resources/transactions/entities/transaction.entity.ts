import { Transaction } from '@prisma/client';
import { Prisma } from '.prisma/client';
import { ApiProperty } from '@nestjs/swagger';

export class TransactionEntity implements Transaction {
  @ApiProperty()
  transactionId: number;

  @ApiProperty()
  amount: Prisma.Decimal;

  @ApiProperty()
  comment: string | null;

  @ApiProperty()
  transactionDate: Date;

  @ApiProperty()
  userId: number;

  @ApiProperty()
  walletId: number;

  @ApiProperty()
  categoryId: number | null;
}
