import { HttpAdapterHost, NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Logger, ValidationPipe } from '@nestjs/common';
import { PrismaClientExceptionFilter } from './prisma/prisma-client-exception.filter';
import * as morgan from 'morgan';
import helmet from 'helmet';
import * as process from 'process';

export function useRequestLogging(app) {
  const logger = new Logger('Request');
  app.use(
    morgan('tiny', {
      stream: {
        write: (message) => logger.log(message.replace('\n', '')),
      },
    }),
  );
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));

  const { httpAdapter } = app.get(HttpAdapterHost);
  app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapter));

  useRequestLogging(app);
  app.enableCors();
  if (process.env.IS_PRODUCTION_ENV !== 'true') {
    app.use((req, res, next) => {
      setTimeout(next, 300);
    });
  }
  const config = new DocumentBuilder()
    .setTitle('Family App')
    .setDescription('The Family App API description')
    .setVersion('0.1')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
  app.use(helmet());

  await app.listen(3000);
}

bootstrap();
